/**
 * Created by saria on 7/11/17.
 */
import React from 'react';
import {BrowserRouter, Route} from 'react-router-dom'
import routeNames from './routeNames'

import UserLoginContainer from '../components/containers/user-login-container.js'
import UserRegisterContainer from '../components/containers/user-register-container.js'
import UserProfileContainer from '../components/containers/user-profile-container.js'
import Main from "../components/layouts/main";
export default (
    <BrowserRouter>
        <Main>
            <Route path={routeNames.login} component={UserLoginContainer}/>
            <Route path={routeNames.register} component={UserRegisterContainer}/>
            <Route path={routeNames.profile} component={UserProfileContainer}/>
        </Main>
    </BrowserRouter>
)