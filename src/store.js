/**
 * Created by saria on 7/11/17.
 */
import { createStore } from 'redux';
import reducers from './reducers';

const store = createStore(reducers);
export default store;
