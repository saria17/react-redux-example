import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux'
import router from './routes/routes'
import store from './store'
import registerServiceWorker from './registerServiceWorker';


ReactDOM.render(<Provider store={store}>{router}</Provider>, document.getElementById('root'));
registerServiceWorker();
