/**
 * Created by saria on 7/11/17.
 */
import * as types from '../actions/action-types'
export function loginUser(user) {
    return {
        type: types.Login_User,
        user
    };

}
export function registerUser(user){
    return {
        type: types.Register_User,
        user
    }
}