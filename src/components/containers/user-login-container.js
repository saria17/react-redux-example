/**
 * Created by saria on 7/11/17.
 */
/**
 * Created by saria on 7/11/17.
 */
import React, {Component} from 'react';
import UserLogin from "../views/user-login";
import * as userApi from '../../api/user-api'
class UserLoginContainer extends Component {
    handleUserLogin=(e)=>{
        e.preventDefault();
        let username=e.target.username.value;
        let password=e.target.password.value;
        userApi.loginUser(username, password);

    };
    render() {
        return (
           <UserLogin onLogin={this.handleUserLogin}/>
        );
    }
}

export default UserLoginContainer;
