/**
 * Created by saria on 7/11/17.
 */
import React from 'react';

const UserLogin = (props) => {

    return (
        <div className="UserLogin">
            <div className="Login-header">
                <h1>Login</h1>
            </div>
            <div className="Login-Panel ">

                <form onSubmit={props.onLogin}>
                    <div className="form-group">
                        <label>User name</label>
                        <input type="text" className="form-control" name="username"/>
                    </div>
                    <div className="form-group">
                        <label>Password</label>
                        <input type="password" className="form-control" name="password"/>
                    </div>
                    <div className="form-group">
                        <button className="btn btn-success">Login</button>
                    </div>
                </form>
            </div>
        </div>
    );

};

export default UserLogin;