/**
 * Created by saria on 7/12/17.
 */
//import axios from 'axios'
import store from '../store'
import * as userActions from '../actions/user-actions'
export function loginUser(username, password) {
   store.dispatch(userActions.loginUser({username, password}))
}